public class Card{
	private Suit suit;
	private Rank rank;
	
	public Card(Suit suit, Rank rank){
		this.suit = suit;
		this.rank = rank;
	}
	
	//getters
	public Suit getSuit(){
		return this.suit;
	}
	public Rank getRank(){
		return this.rank;
	}
	
	public double calculateScore(){
		double valueOfCard = 0;
		
		valueOfCard += this.suit.getScore() + this.rank.getScore();
		
		return valueOfCard;
	}
	
	//toString
	public String toString(){
		String output = "";
		
		//Text colours and background clours
		final String blackText = "\u001B[30m";
		final String whiteText = "\u001B[37m";	
		final String redText = "\u001B[31m";	
		final String blackBackground = "\u001B[40m";
		final String whiteBackground = "\u001B[47m";
		String cardColour = blackText;
		
		int cardHeight = 11; 

		String[] rows = new String[cardHeight];
		
		//Prints the card with the same colour as the backgound unless it's the middle where the card will be black or red depending on the suit
		for(int i = 0; i < cardHeight; i++){								
			if(this.suit == Suit.HEARTS || this.suit == Suit.DIAMONDS)
				cardColour = redText;
			else 
				cardColour = blackText;				
		
			if(i == cardHeight/2)
				rows[i] += " " + whiteBackground + whiteText + cardColour + " " + this.rank + " of " + this.suit + " " + blackBackground;
			else
				rows[i] += " " + whiteBackground + whiteText + " " + this.rank + " of " + this.suit  + " " + blackBackground;							
		}		
		
		for(int i = 0; i < cardHeight; i++){
			output += rows[i].substring(4) + "\n";
		}			
		
		return output;
	}
}