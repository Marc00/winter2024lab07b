import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		
		this.numberOfCards = 52;
		
		cards = new Card[this.numberOfCards];
		
		int o = 0;
		
		//This goes through the full numbers array for every suit in the suit array making the 52 unique cards
		for(Suit s : Suit.values()){
			for(Rank r : Rank.values()){
				this.cards[o] = new Card(s, r);
				o++;
			}
		}
	}
	
	//Getter
	public int getNumberOfCards(){
		return this.numberOfCards;
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	//Return the last card of the deck and reduce the size of the deck by 1
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	
	//Lists the cards of the deck one under the other
	public String toString(){
				
		String output = "";
		
		for(int i = 0; i < this.numberOfCards; i++){
			output += cards[i] + "\n" ;
		}		
		
		return output;
	}
	
	//Goes through every index and switches the card at each index with one at a random index
	public void shuffle(){
		for(int i = 0; i < this.numberOfCards - 1; i++){
			int randomPosition = this.rng.nextInt(this.numberOfCards);
			
			Card hold = this.cards[i]; 
			this.cards[i] = this.cards[randomPosition];
			this.cards[randomPosition] = hold;
		}
	}
}